import React from 'react';
import styles from './GamePage.module.scss';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import DeckComponent from '../../components/deck/DeckComponent';
import DealerComponent from '../../components/dealer/DealerComponent';
import PlayerComponent from '../../components/player/PlayerComponent';
import GamblerComponent from '../../components/gambler/GamblerComponent';

const HomePage = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const deck = useAppSelector(GameSelector.deck);
  const scene = useAppSelector(GameSelector.scene);

  React.useEffect(() => {
    switch (scene) {
      case 'BET':
        dealCards();
        break;
    }
  }, [scene]);
 
  const dealCards = () => {
    const cards = deck.slice(0, 8);
    cards.map((card, index) => {
      new Promise<void>(resolve => setTimeout(() => {
        dispatch(GameAction.drawCard({ gamblerIndex: index % 4, card: card }));
        resolve();
      }, 300 * (index + 1)));
    });

    dispatch(GameAction.changeScene('DEAL'));
  };

  return (
    <main className={styles.main}>
      <div className={`${styles.react} container`}>
        <div className="row">
          <div className="col-lg-8 col-md-6">
            <DeckComponent />
          </div>
          <div className="col-lg-4 col-md-6">
            <DealerComponent />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <PlayerComponent />
          </div>
          <div className="col-lg-4 col-md-6">
            <GamblerComponent gamblerIndex={1} />
          </div>
          <div className="col-lg-4 col-md-6">
            <GamblerComponent gamblerIndex={2}/>
          </div>
        </div>
      </div>
    </main>
  );
};

export default HomePage;
