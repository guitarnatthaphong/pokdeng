import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import gameReducer, { GAME_STORE_NAMESPACE } from './game/gameSlice';

export const store = configureStore({
  reducer: {
    [GAME_STORE_NAMESPACE]: gameReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
export type RootState = ReturnType<typeof store.getState>;
