import React from 'react';
import styles from './LayoutComponent.module.scss';
import { Link, Outlet } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import Swal, { SweetAlertOptions } from 'sweetalert2';
import { ToastContainer } from 'react-toastify';

const LayoutComponent = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const scene = useAppSelector(GameSelector.scene);
  const options: SweetAlertOptions = {
    title: 'คำเตือน',
    text: 'คุณต้องการเริ่มเกมใหม่ใช่ไหม ?',
    icon: 'warning',
    confirmButtonColor: '#f86c6b',
    showCancelButton: true,
    focusCancel: true,
  };

  const startClick = async () => {
    if (scene !== 'SLEEP') {
      const result = await Swal.fire(options);

      if (result.isConfirmed) {
        dispatch(GameAction.resetGame());
        setTimeout(() => dispatch(GameAction.newGame()), 0);
      }
    } else {
      dispatch(GameAction.newGame());
    }

    dispatch(GameAction.changeScene('START'));
  };

  return (
    <div className={styles.layout}>
      <header>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            <span className="fa fa-heart me-2"></span>ป๊อกเด้ง
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
            <button
              type="button"
              className="btn btn-success"
              onClick={startClick}
            >
              เริ่มเกมใหม่
            </button>
          </div>
        </div>
        </nav>
      </header>

      <Outlet />
      <ToastContainer position="bottom-right" theme="colored" autoClose={10000} />
    </div>
  );
};
  
  export default LayoutComponent;
  