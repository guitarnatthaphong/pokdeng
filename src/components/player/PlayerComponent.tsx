import React from 'react';
import styles from './PlayerComponent.module.scss';
import { useAppSelector } from '../../hooks/redux';
import { GameSelector } from '../../store/game/gameSlice';
import type { Ref } from '../modal/ref';
import CardComponent from '../card/CardComponent';
import ControlComponent from '../control/ControlComponent';
import CriminalModalComponent from '../criminal/CriminalModalComponent';

const PlayerComponent = (): JSX.Element => {
  const gamblers = useAppSelector(GameSelector.gamblers);
  const player = gamblers.length > 0
    ? gamblers[0]
    : { name: '???', aka: '?????', cards: [], coins: 0, bet: 0, criminals: [] };
  const criminalModal = React.useRef<Ref>(null);

  const handleCriminalClick = () => {
    criminalModal.current?.show();
  };

  return (<>
    <div className={`${styles.card} card mb-3`}>
      <div className="card-body">
        <div className="row">
          <div className="col-6">
            <h5 className="card-title">
            <span className="fa fa-user me-2"></span>{player.name}
          </h5>
          <h6 className="card-subtitle mb-2 text-muted">{player.aka}</h6>
          </div>
          <div className="col-6 d-flex justify-content-end">
            <h1>{player.bet}</h1>
          </div>
        </div>
        <div className="d-flex justify-content-between mb-1">
          <button
            className="btn btn-outline-danger btn-sm"
            disabled={player.criminals.length === 0}
            onClick={handleCriminalClick}
          >
            <span className="fa fa-skull me-2"></span>
            <span>{player.criminals.length}</span>
          </button>
          <div className="d-flex align-items-center">
            <span className="text-warning fa fa-coins me-2"></span>
            <span>{player.coins}</span>
          </div>
        </div>
        <div className="row mb-2">
          {[...Array(3)].map((_, index) =>
            <div key={index} className="col-4 d-flex justify-content-center">
              <CardComponent
                gamblerIndex={0}
                cardIndex={index}
                card={player.cards.length > index ? player.cards[index] : null}
              />
            </div>
          )}
        </div>
        <div className="control-panel">
          <ControlComponent />
        </div>
      </div>
    </div>

    {/* Modals */}
    <CriminalModalComponent
      ref={criminalModal}
    />
  </>);
};

export default PlayerComponent;
