import React from 'react';
import styles from './CriminalModalComponent.module.scss';
import Modal from 'react-bootstrap/Modal';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import type { Ref } from '../modal/ref';

const CriminalModalComponent = React.forwardRef<Ref>((_, ref) => {
  React.useImperativeHandle(ref, () => ({
    show: handleShow,
    hide: handleClose,
  }));

  const [show, setShow] = React.useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  return (
    <Modal
      ref={ref}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <h5 className="modal-title">
          <span className="fa fa-skull me-2"></span>ประวัติอาชญากรรม
        </h5>
      </Modal.Header>
      <Modal.Body>
        <div className="dropdown">
          <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Case
          </button>
          <ul className="dropdown-menu">
            <li><a className="dropdown-item" href="#">stealing from homeless people</a></li>
            <li><a className="dropdown-item" href="#">stealing mom credit card</a></li>
            <li><a className="dropdown-item" href="#">sold your dead friend watch's</a></li>
          </ul>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={handleClose}
        >
          <span className="fas fa-fw fa-ban me-2"></span>ปิด
        </button>
      </Modal.Footer>
    </Modal>
  );
});

CriminalModalComponent.displayName = 'CriminalModal';
export default CriminalModalComponent;
