import React from 'react';
import styles from './FinishControlComponent.module.scss';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import { toast } from 'react-toastify';
import MessageComponent from '../message/MessageComponent';

const FinishControlComponent = () => {
  const dispatch = useAppDispatch();
  const gamblers = useAppSelector(GameSelector.gamblers);
  const points = useAppSelector(GameSelector.points);

  const handleEndClick = () => {
    const dealerPoints = points[3].point;
    const dealerDeng = points[3].deng;
    const gamblerPoints = points.filter((_, index) => index < 3);

    gamblerPoints.map((gamblerPoint, index) => {
      if (gamblerPoint.point > dealerPoints) {
        const reward = gamblers[index].bet * (2 + (gamblerPoint.deng - 1 > 0 ? gamblerPoint.deng - 1 : 0));
        toast.success(`${gamblerPoint.name} [ชนะ] ได้ ${reward}`);
        dispatch(GameAction.gameResult({ gamblerIndex: index, coins: reward }));
      } else if (gamblerPoint.point < dealerPoints) {
        const waste = gamblers[index].bet * (1 + (dealerDeng * 1 > 0 ? dealerDeng * 1 : 0));
        toast.error(`${gamblerPoint.name} [แพ้] เสีย ${waste}`);
        dispatch(GameAction.gameResult({ gamblerIndex: index, coins: -waste }));
      } else {
        const back = gamblers[index].bet;
        dispatch(GameAction.gameResult({ gamblerIndex: index, coins: back }));
      }
    });

    dispatch(GameAction.clearDeck());
    dispatch(GameAction.changeScene('START'));
  };

  const toMessage = (point: number, deng: number) => {
    const dealerPoint = points[3].point;
    const messagePoint = point >= 8 ? `ป๊อก ${point}` : `${point} แต้ม`;
    const messageDeng = deng > 0 ? `, ${deng} เด้ง` : '';
    const score = point > 0 ? `${messagePoint}${messageDeng}` : 'บอด';
    return `${dealerPoint >= point ? (dealerPoint === point ? '[เสมอ]' : '[แพ้]') : '[ชนะ]'} ${score}`;
  };

  return (
    <div className="row">
      <div className="col-9">
        <MessageComponent message={toMessage(points[0].point, points[0].deng)} />
      </div>
      <div className="col-3 d-grid gap-2">
        <button
          className="btn btn-primary"
          onClick={handleEndClick}
        >
          จบเกม
        </button>
      </div>
    </div>
  );
};

export default FinishControlComponent;
