import React from 'react';
import styles from './ControlComponent.module.scss';
import { useAppSelector } from '../../hooks/redux';
import { GameSelector } from '../../store/game/gameSlice';
import MessageComponent from '../../components/message/MessageComponent';
import BetControlComponent from './BetControlComponent';
import DealControlComponent from './DealControlComponent';
import CheckControlComponent from './CheckControlComponent';
import DrawControlComponent from './DrawControlComponent';
import FinishControlComponent from './FinishControlComponent';

const ControlComponent = (): JSX.Element => {
  const scene = useAppSelector(GameSelector.scene);

  const controls = () => {
    switch (scene) {
      case 'SLEEP':
        return (<MessageComponent message='' />);
      case 'START':
        return (<BetControlComponent />);
      case 'DEAL':
        return (<DealControlComponent />);
      case 'CHECK':
        return (<CheckControlComponent />);
      case 'DRAW':
        return (<DrawControlComponent />);
      case 'FINISH':
        return (<FinishControlComponent />);
    }
  };

  return (<>{controls()}</>);
};

export default ControlComponent;
