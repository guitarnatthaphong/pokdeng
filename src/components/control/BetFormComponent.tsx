import React from 'react';
import styles from './BetFormComponent.module.scss';

type Props = {
  coins: number,
  onSelect: React.Dispatch<number>,
};

const BetFormComponent = (props: Props): JSX.Element => {
  const coins: number[] = [];
  while (coins.reduce((sum, coin) => sum + coin, 0) !== props.coins) {
    coins.push(10);
  }
  const choices = coins.map((coin, index) => coin * (index + 1));

  return (
    <div className="d-grid gap-1">
      {choices.map((choice, index) => 
        <button
          key={index}
          className="btn btn-primary"
          onClick={() => props.onSelect(choice)}
        >
          <span className="fa fa-coins me-2"></span>{choice}
        </button>
      )}
    </div>
  );
};

export default BetFormComponent;
